﻿namespace ScriptTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSimpleDemo = new System.Windows.Forms.Button();
            this.btnTemplateDemo = new System.Windows.Forms.Button();
            this.btnTypeMappingDemo = new System.Windows.Forms.Button();
            this.btnMethodMappingDemo = new System.Windows.Forms.Button();
            this.btnTypeProxyDemo = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnTypeProxyDemo);
            this.groupBox1.Controls.Add(this.btnMethodMappingDemo);
            this.groupBox1.Controls.Add(this.btnTypeMappingDemo);
            this.groupBox1.Controls.Add(this.btnSimpleDemo);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(482, 118);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "脚本执行";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnTemplateDemo);
            this.groupBox2.Location = new System.Drawing.Point(12, 136);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(482, 104);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "模板生成";
            // 
            // btnSimpleDemo
            // 
            this.btnSimpleDemo.Location = new System.Drawing.Point(22, 30);
            this.btnSimpleDemo.Name = "btnSimpleDemo";
            this.btnSimpleDemo.Size = new System.Drawing.Size(92, 60);
            this.btnSimpleDemo.TabIndex = 0;
            this.btnSimpleDemo.Text = "简单例子";
            this.btnSimpleDemo.UseVisualStyleBackColor = true;
            this.btnSimpleDemo.Click += new System.EventHandler(this.btnSimpleDemo_Click);
            // 
            // btnTemplateDemo
            // 
            this.btnTemplateDemo.Location = new System.Drawing.Point(22, 29);
            this.btnTemplateDemo.Name = "btnTemplateDemo";
            this.btnTemplateDemo.Size = new System.Drawing.Size(92, 60);
            this.btnTemplateDemo.TabIndex = 1;
            this.btnTemplateDemo.Text = "模板生成";
            this.btnTemplateDemo.UseVisualStyleBackColor = true;
            this.btnTemplateDemo.Click += new System.EventHandler(this.btnTemplateDemo_Click);
            // 
            // btnTypeMappingDemo
            // 
            this.btnTypeMappingDemo.Location = new System.Drawing.Point(137, 30);
            this.btnTypeMappingDemo.Name = "btnTypeMappingDemo";
            this.btnTypeMappingDemo.Size = new System.Drawing.Size(92, 60);
            this.btnTypeMappingDemo.TabIndex = 1;
            this.btnTypeMappingDemo.Text = "类型映射";
            this.btnTypeMappingDemo.UseVisualStyleBackColor = true;
            this.btnTypeMappingDemo.Click += new System.EventHandler(this.btnTypeMappingDemo_Click);
            // 
            // btnMethodMappingDemo
            // 
            this.btnMethodMappingDemo.Location = new System.Drawing.Point(367, 30);
            this.btnMethodMappingDemo.Name = "btnMethodMappingDemo";
            this.btnMethodMappingDemo.Size = new System.Drawing.Size(92, 60);
            this.btnMethodMappingDemo.TabIndex = 2;
            this.btnMethodMappingDemo.Text = "方法/属性映射";
            this.btnMethodMappingDemo.UseVisualStyleBackColor = true;
            this.btnMethodMappingDemo.Click += new System.EventHandler(this.btnMethodMappingDemo_Click);
            // 
            // btnTypeProxyDemo
            // 
            this.btnTypeProxyDemo.Location = new System.Drawing.Point(252, 30);
            this.btnTypeProxyDemo.Name = "btnTypeProxyDemo";
            this.btnTypeProxyDemo.Size = new System.Drawing.Size(92, 60);
            this.btnTypeProxyDemo.TabIndex = 3;
            this.btnTypeProxyDemo.Text = "类型代理";
            this.btnTypeProxyDemo.UseVisualStyleBackColor = true;
            this.btnTypeProxyDemo.Click += new System.EventHandler(this.btnTypeProxyDemo_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 252);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "ScriptParser Demo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSimpleDemo;
        private System.Windows.Forms.Button btnTemplateDemo;
        private System.Windows.Forms.Button btnMethodMappingDemo;
        private System.Windows.Forms.Button btnTypeMappingDemo;
        private System.Windows.Forms.Button btnTypeProxyDemo;
    }
}